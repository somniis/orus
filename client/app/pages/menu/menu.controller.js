'use strict';

angular.module('nixApp')
  .controller('MenuCtrl', function ($scope,$rootScope, $state,$location,$timeout,
    $mdSidenav,$q, $mdUtil, $log,bifrost,DTDefaultOptions,DTOptionsBuilder,
    LxNotificationService,LxProgressService,global ) {

   //Cerrar sesión
    $scope.closeSession = function () {
      LxNotificationService.confirm('¿Deseas cerrar sesión?', 'Se perderá el cahcé ' +
      'y las preferencias', { cancel:'Cancelar', ok:'Aceptar' },
      function(answer){
        if (answer) {
          global.dStor(['nombre']);
          $state.go('login');
        }
      });
    };
    //

    $scope.index = function(){
      $state.go(global.gStor('index'));
    };

    $scope.menuHeads = [
      {
        'sh' : false,
        'icon' : 'ticket',
        'title': 'Ticket',
        'menuItems': [{
          'title' : 'Nuevo Ticket',
          'url' : 'm.newTicket'
        },{
          'title' : 'Administrar Ticket',
          'url' : 'm.adminTicket'
        }]
      },{
        'sh' : false,
        'icon' : 'apps',
        'title': 'Inventario',
        'menuItems': [{
          'title' : 'Nuevo activo',
          'url' : 'm.newActive'
        },{
          'title' : 'Administrar activos',
          'url' : 'm.adminActive'
        }]
      },{
        'sh' : false,
        'icon' : 'book',
        'title': 'Clientes',
        'url' : 'm.mc.addC',
      },{
        'sh' : false,
        'icon' : 'key',
        'title': 'Usuarios',
        'url' : 'm.mu.addU',
      },{
        'sh' : false,
        'icon' : 'account-multiple',
        'title': 'Proveedores',
        'url' : 'm.adminVendors',
      },
      {
        'sh' : false,
        'icon' : 'chart-arc',
        'title': 'Estadisticas y reportes',
        'url' : 'm.charts',
      }
    ];

    ///MENU SIDEBAR
    $scope.SinMenu = function () {
      return false;
    };
    $scope.isActive = function(route) {
      return route === $location.path();
    };
    function buildToggler(navID) {
      var debounceFn =  $mdUtil.debounce(function(){
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                $log.debug('toggle ' + navID + ' is done');
              });
          },300);
      return debounceFn;
    }
    $scope.toggleLeft = buildToggler('left');


  }).controller('LeftCtrl', function ($scope, $timeout, $mdSidenav) {
   $scope.close = function () {
     $mdSidenav('left').close();
   };
 });
