'use strict';

angular.module('nixApp')
  .controller('MasterCtrl', function ($scope,$rootScope,global,charts) {
    $rootScope.tit='Inicio';
    //
    $scope.MC = {}; //MASTER CHART
    $scope.MC.CTET = {}; //Chart Ticket x Estado x Tecnico
    charts.oCE({
      table : 'tecnicos',
      label : 'nombre',
      id : 'id_persona'
    }).then(function(ch){
        $scope.MC.CTET = {//Chart Ticket x Estado x Tecnico
          'labels' : ch.labels,
          'data' : ch.data,
          'series' : ch.series
        };
    });

    //CREADOS POR FECHA MES AÑO DIA
    $scope.MC.TTG = {}; // Total tickets general
      charts.oCD({
        year : '2015'
      }).then(function(ch){
        console.log(ch);
        $scope.MC.TTG = {//Chart Ticket x Estado x Tecnico
          'labels' : ch.labels,
          'data' : ch.data
        };
      });
  });
