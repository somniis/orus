'use strict';

angular.module('nixApp')
  .controller('NewTicketCtrl', function ($q,$scope,$rootScope,$location,
    bifrost,LxNotificationService,LxProgressService,LxDialogService,global) {
      $rootScope.tit="Nuevo ticket";
    //ACTIVAR EL MENU BAR
    //$rootScope.menuVisible($location.$$path);
    //INICIA EN CONTROLADOR
    function init_controller (){
      return $q(function(resolve){
        //DESACTIVA LAS OPCIONES DE RADIO BUTTON
        $scope.areaLista = false;
        //OBJETO SEL CARGA DE COMBOS
        $scope.sel ={};
        // OBJETO ARRT = ArrayTicket = Objeto de descarga
        $scope.arrT = {};
        //OBJETO DE RADIOS
        $scope.r = 1;
        //MUESTRA LOS ACTIVOS
        $scope.showAc = false;
        // MODELO DT PARA TABLA
        $scope.dt = {};
        //carga COMBOS
        $rootScope.cargaCombo("servicio","*").then(function(r){$scope.sel.servicio =  r;});
        $rootScope.cargaCombo("mesa_servicio","*").then(function(r){$scope.sel.mesa =  r;});
        $rootScope.cargaCombo("prioridad","*").then(function(r){$scope.sel.prioridad =  r;});
        $rootScope.cargaCombo("tecnicos","*").then(function(r){$scope.sel.tecnico =  r;});
        $rootScope.cargaCombo("empresa_sede","*").then(function(r){$scope.sel.sede =  r;});
        $rootScope.cargaCombo("original","*").then(function(r){$scope.sel.original =  r;});
        resolve('return');
      });
    }
    //Inicia por primera vez en controlador
    init_controller();
    //SCOPE FUNCTIONES
    $scope.cargaAreas = function(newV){
      $scope.arrT.area = undefined;
      $scope.arrT.usuario = undefined;
      var id_sede = newV.newValue.id_sede;
      $rootScope.cargaCombo("area_sedes","*"," fk_id_sede = "+ id_sede ).then(function(r){
        $scope.sel.area = r;
        $rootScope.cargaCombo("sede_contacto","*"," fk_id_sede = "+ id_sede ).then(function(r){
          $scope.sel.usuario = r;
        });
      });
    };
    //VERIFICAR SI HAY AREA
    $scope.$watch('arrT.area',function(value){
      if (!value) {
        $scope.areaLista = false;
        $scope.showAc = false;
        $scope.r = 1;
      }
    });
    //cuando cambia area activo radios
    $scope.changeArea  = function(value){
        $scope.r = true;
        $scope.areaLista = true;
    }
    //-----------------------------------------------------------------------------------------------------------------------TABLA
      //CARGA INVENTARIO sea propio o del cliente
      function cargarInventario (arg){
        return $q(function(resolve){
          bifrost.sParams({"table":"inventarioActivos","val" : arg.th,"where":"Placa_inv is "+ arg.not +" null and id_area_sede =  "+ $scope.arrT.area.id_area_sede});
          bifrost.s().then(function(dat){
            $scope.tFilter = arg.th.split(",");
            $scope.tHead = arg.head;
            $scope.tBody = dat;
            resolve("ya");
          });
        });
      };
      //Configuración inicial de la tabla

      $scope.tHead = ["init"];
      $scope.tFilter = ["init"];
      $scope.tBody = [{"init":"nuller"}];
      $scope.arrT.activo = 0;
      $scope.changeSel = function(id){
        $scope.arrT.activo = id;
      };
      //MUESTRA EL ACTIVO SEGUN EL RADIO ACTIVO
      $scope.$watch('r', function(value) {
        if (value == 1){
            $scope.showAc = false;
            $scope.arrT.activo = 0;
        }
        else if (value == 2){//ACTIVO CLIENTE
          cargarInventario({
            "th" :  "id_activo,Activo,Serial,Marca,Modelo",
            "head" : ["id_activo","Activo","Serial","Marca","Modelo"],
            "not" : " "
          }).then(function(d){
            $scope.showAc = true;
          });
        }
        else if (value == 3){//ACTIVO PROPIO
          cargarInventario({
            "th" :  "id_activo,Activo,Serial,Marca,Modelo,Placa_inv,Placa_seguridad",
            "head" : ["id_activo","Activo","Serial","Marca","Modelo","Placa inv","Placa seg"],
            "not" : "not"
          }).then(function(d){
            $scope.showAc = true;
          });
        }
      });
      //OPEN PPICKER CON PRIORIDAD
      $scope.openDateP = function(){
        var el = document.getElementById('fecha');
        angular.element(el).triggerHandler('click');
      };
      //CONFIRMACION ACTIVO
      $scope.showConfirm = function(){
        if(Object.keys($scope.arrT).length > 10)
          if($scope.r == 1) { //SIN ACTIVO
              LxDialogService.open('confirm');
          }
          else { // CON ACTIVO
            if($scope.arrT.activo == 0)//ACTIVO NO SELECCIONADO
              LxNotificationService.warning('Debes seleccionar un activo');
            else // ACTIVO SELECCIONADO
              LxDialogService.open('confirm');
          }
        else
          LxNotificationService.warning('Debes completar el formulario para poder crear el ticket');
      };

      // ACEPTAR ACTIVO
      $scope.createTicket = function(){
        LxProgressService.circular.show('#5fa2db', '#progress');
        if($scope.arrT.activo === 0)
          $scope.arrT.activo = {"id_activo" : "1"};

        var dat = $scope.arrT;
        //Date Now
        var dN = new Date();
        var uStor = global.gStor(["id_usuario","nombre"]);
        var nombre_persona = uStor.nombre;
        var id_usuario = uStor.id_usuario;
        var hora = dN.getHours() + ":" + dN.getMinutes();
        //Fecha Config
        var fecha = global.cD(dat.fecha);

        bifrost.sParams({
          "Procedure":"RealizarTicket",
          "val": global.rQ([
            dat.mesa.mesa,
            dat.descripcion,
            fecha ,
            dat.original.atencion,
            dat.descripcion,
            dat.tecnico.id_persona,
            id_usuario,
            dat.area.id_area_sede,
            dat.prioridad.id_prioridad,
            hora,
            dat.activo.id_activo,
            dat.servicio.id_servicio,
            nombre_persona
            // PENDIENTE : FIRMA
          ])
        });
        bifrost.eE().then(function(res){
          //renicio en controlador
          LxDialogService.close('confirm');
          init_controller().then(function(){
            //Una vez iniciado se muestra
            LxProgressService.circular.hide();
            LxNotificationService.success('¡El ticket numero se ha realizado!');
          });
        },function(err){
          LxProgressService.circular.hide();
          LxNotificationService.warning('¡Algo ha fallado!');
        });
      };

  });
