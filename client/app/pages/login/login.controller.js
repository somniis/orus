'use strict';

angular.module('nixApp')
  .controller('LoginCtrl', function ($scope,$rootScope,$http,$state,$location,/*service*/bifrost,global,LxNotificationService) {
    $rootScope.tit = 'Login';
    $scope.data = {};
    $scope.login = function () {
      $rootScope.showProg();
      bifrost.sParams({
        'table': 'UsuarioPersona',
        'val' : ' * ',
        'where' : ' nickname =  \'' + $scope.data.usu  +'\' and contrasena = \''+ $scope.data.pass +'\' '
      });
      bifrost.s().then(function(dat){
        if (dat[0]){
          global.sStor(dat[0]).then(function(){
            global.dStor(['contrasena']);
            if ($scope.chGuardar) {
              global.sStor({
                'usuario':$scope.data.usu,
                'contrasena':$scope.data.pass
              });
            }

            $rootScope.per =  $rootScope.firstName();

            $rootScope.hideProg();
            var ind = '';
            if (dat[0].fk_id_rol == 1)
              ind = 'm.Master';
            else if (dat[0].fk_id_rol == 6)
              ind = 'm.Admin'
            else if (dat[0].fk_id_rol == 5)
              ind = 'm.Tech'
            else if (dat[0].fk_id_rol == 7)
              ind = 'm.Client'


            global.sStor({
              'index' : ind
            }).then(function(){

              $state.go(ind);
            });

          });
        }
        else{
          LxNotificationService.warning('Credenciales incorrectas');
          $rootScope.hideProg();
        }
      });
    };
  });
