'use strict';

angular.module('nixApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('entrega', {
        url: '/entrega',
        templateUrl: 'app/pages/inventario/entrega/entrega.html',
        controller: 'EntregaCtrl'
      });
  });