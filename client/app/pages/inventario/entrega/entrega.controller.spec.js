'use strict';

describe('Controller: EntregaCtrl', function () {

  // load the controller's module
  beforeEach(module('nixApp'));

  var EntregaCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EntregaCtrl = $controller('EntregaCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
