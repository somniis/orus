'use strict';

angular.module('nixApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('adminActive', {
        url: '/adminActive',
        templateUrl: 'app/pages/inventario/adminActive/adminActive.html',
        controller: 'AdminActiveCtrl'
      });
  });