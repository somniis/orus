'use strict';

angular.module('nixApp')
  .controller('NewActiveCtrl', function ($scope,$rootScope,global,OPCIONES,bifrost) {
    $rootScope.tit="Nuevo activo";
    //Instancia principal  New Active
    $scope.NA = {"f": { "r" : "P" }}; // f = formularios
    //ARRAY ESPECIFICACIONES
    $scope.NA.arrEsp = [];
    //SELECTS
    $scope.sel = {};
    //carga COMBOS
    $rootScope.cargaCombo("empresa_sede","*").then(function(r){$scope.sel.sede = r});
    $rootScope.cargaCombo("tipo_activo","*").then(function(r){$scope.sel.tipoA = r});
    //FUNCIONES SELECT
    $scope.openArea = function(values){
      var id_sede = values.newValue.id_sede;
      $rootScope.cargaCombo("area_sedes","*"," fk_id_sede = "+ id_sede ).then(function(ro){$scope.sel.area  = ro;});
    };
    //
    var activoEsp  = false;
    //SE ABREN ESPECIFICACIONES
    $scope.openSpecs = function(values){
      var id = values.newValue.id_tipo_activo;
      $rootScope.cargaCombo("EspCampo","*"," fk_id_tipo_activo = "+ id).then(function(ro){
        if(ro.length > 0){
          $scope.NA.arrEsp = ro[0].campos.split('*');
          activoEsp = true;
        }
        else{
          $scope.NA.arrEsp = [];
          activoEsp = false;
        }
      });
    };
    //FUNCION REPETICION
    function rep (){
      var f = $scope.NA.f;
      var obj = [
        {"name":"serial","value":f.serial},
        {"name":"placa_inv","value":f.placaInv},
        {"name":"placa_seguridad","value":f.placaSeg}
      ];
      global.vR(obj,"activo");/*.then(function(response){
        if (response[0]) {
          global.not(response[1]);
          return true;
        }
        else
          return false
      });*/
    }
    //FUNCION INSERTAR ACTIVO
    function newActiveF(val) {
      for (var i = 0; i < val.length; i++)
        if(val[i] == undefined)
          val[i] = "'NULL'"

      bifrost.sParams({
        "Procedure" : "newActive",
        "val" : val
      }).then(function(){

      });
    }
    //
    $scope.newActive = function(){
      // EN CASO QUE SEA UN ACTIVO CON ESPECIFICACIONES
      var NA = $scope.NA.f;
      var leng = Object.keys(NA).length;
      for (var i = 0; i < OPCIONES.length; i++){
        if(OPCIONES[i].var == NA.r){
          if(OPCIONES[i].num == (leng - 1)){
            if (activoEsp) { // ACTIVO CON ESPECIFICACIONES
              console.log('CON');
              if ($scope.NA.arrEsp.texto) {
                if ($scope.NA.arrEsp.length == Object.keys($scope.NA.arrEsp.texto).length ) {
                  if (!rep()) {
                    var espT = "";
                    for (var x = 0; x < $scope.NA.arrEsp.texto.length; x++){
                      espT += $scope.NA.arrEsp.texto[x] + "*";
                    }
                    console.log(espT);
                    /*newActiveF(global.rQ([
                      NA.serial,
                      NA.marca,
                      NA.modelo,
                      NA.placaInv,
                      NA.placaSeg,
                      NA.tipoA.id_tipo_activo,
                      espT,
                      NA.r,
                      NA.area.id_area_sede
                    ]));*/
                  }
                }else {
                    global.not('El activo está compuesto por especificaciones, debes completar estas.');
                }
              }else {
                  global.not('El activo está compuesto por especificaciones, debes completar estas.');
              }
            }else{ // ACTIVO SIN ESPECIFICAIONES
              /*if (!rep()) {
                newActiveF(global.rQ([
                  NA.serial,
                  NA.marca,
                  NA.modelo,
                  NA.placaInv,
                  NA.placaSeg,
                  NA.tipoA.id_tipo_activo,
                  "0",
                  NA.r,
                  NA.area.id_area_sede
                ]));
              }*/
              console.log('SIN ');
            }
          }else{
              global.not('Verifica si el formulario está completo.');
          }
        }
      }
    };
  });
