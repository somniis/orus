'use strict';

describe('Controller: NewActiveCtrl', function () {

  // load the controller's module
  beforeEach(module('nixApp'));

  var NewActiveCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewActiveCtrl = $controller('NewActiveCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
