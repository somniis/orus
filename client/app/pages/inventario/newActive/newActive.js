'use strict';

angular.module('nixApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('m.newActive', {
        url: '/newActive',
        templateUrl: 'app/pages/inventario/newActive/newActive.html',
        controller: 'NewActiveCtrl'
      });
  }).constant('OPCIONES',[
    {'var':'B','num':'6'},
    {'var':'P','num':'8'},
    {'var':'C','num':'6'}
  ]
  );
