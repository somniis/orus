'use strict';

describe('Controller: RetiroCtrl', function () {

  // load the controller's module
  beforeEach(module('nixApp'));

  var RetiroCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RetiroCtrl = $controller('RetiroCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
