'use strict';

angular.module('nixApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('retiro', {
        url: '/retiro',
        templateUrl: 'app/pages/inventario/retiro/retiro.html',
        controller: 'RetiroCtrl'
      });
  });