'use strict';
/**
  BRIDGE CONNECTION NODE ______-----_____ ANGULAR
*/
angular.module('nixApp')
  .service('bifrost', function ($http,$q,LxNotificationService) {
    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
    var params = {}; // VAL // TABLE // WHERE // PROCEDURE NAME
    //INTERNO
    function not(note){
      LxNotificationService.error(note);
    }
    //prePost
    function preP (url,params){
      //var pa = CryptoJS.AES.encrypt(params, "NixKey2512");
      return $q(function(resolve,reject){
        $http.post(url,params).then(function(response){

          if(!response.data)
            reject(false);
          else
            resolve(response.data);
        },function(response){
          not('ERROR CONECCTION POST RETURN');
          reject(response.status);
        });
      });
    }
    /*select */
    var select = function (p){
      return $q(function(resolve,reject){
        if (p)
          setParams(p);

        preP('/api/connections/q',{
          'sentence': 'select ' + params.val.toString() + ' from ' + params.table + ' where ' + params.where
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE SELECT');
        },function(op){
          not('ERROR CONECCTION ON SELECT');
          reject(op);
        });
      });
    };
    var update = function (p){
      return $q(function(resolve,reject){
        if (p)
          setParams(p);

        preP('/api/connections/u',{
          'sentence': 'update ' + params.table + ' set ' + params.val.toString()  + ' where ' + params.where
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE UPDATER');
        },function(op){
          not('ERROR CONECCTION ON SUPER TRUNCE UPDATER');
          reject(op);
        });
      });
    };
    var insert = function (p){
      return $q(function(resolve,reject){
        if (p)
          setParams(p);

        preP('/api/connections/i',{
          'sentence': 'insert into ' + params.table  + ' values (' + params.val.toString() + ')'
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE INSERTER');
        },function(op){
          not('ERROR CONECCTION ON SUPER TRUNCE INSERTER');
          reject(op);
        });
      });
    };
    var rExecute = function (p){
      return $q(function(resolve,reject){
        if (p)
          setParams(p);

        preP('/api/connections/rP',{
          'sentence': 'exec  ' + params.Procedure  + ' ' + params.val.toString()
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE RE');
        },function(op){
          not('ERROR CONECCTION ON SUPER TRUNCE RE');
          reject(op);
        });
      });
    };

    var eExecute = function (p){
      return $q(function(resolve,reject){
        if (p)
          setParams(p);

        preP('/api/connections/eP',{
          'sentence': 'exec  ' + params.Procedure  + ' ' + params.val.toString()
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE EE');
        },function(op){
          not('ERROR CONECCTION ON SUPER TRUNCE EE');
          reject(op);
        });
      });
    };

    var setParams = function(p){
      return $q(function(resolve){
        if(p.where === undefined)
          p.where = ' 1=1 ';
        for (var o in p){
          if(p[o] instanceof Array)
            p[o] = p[o].toString()
          p[o] = p[o].replace(/--|select|insert|update|delete|exec|create/g,'');
        }
        params = p;
        resolve('ya');
      });
    };

    var newSelect = function (p){
      return $q(function(resolve,reject){
        if(p.where == undefined)
          p.where = ' 1=1 ';
        for (var o in p){
          if(p[o] instanceof Array)
            p[o] = p[o].toString()
          p[o] = p[o].replace(/--|select|insert|update|delete|exec|create/g,'');
        }
        params = p;
        //
        preP('/api/connections/q',{
          'sentence': 'select ' + params.val.toString() + ' from ' + params.table + ' where ' + params.where
        })
        .then(function(ep){
          if (ep)
            resolve(ep);
          else
            not('ERROR CONECCTION ON SUPER TRUNCE SELECT');
        },function(op){
          not('ERROR CONECCTION ON SELECT');
          reject(op);
        });



      });
    };

    return {
      s: select,
      i: insert,
      u: update,
      rE: rExecute,
      eE: eExecute,
      sParams : setParams,
      gParams : function(){
        return params;
      },
      ns : newSelect
    };
  });
