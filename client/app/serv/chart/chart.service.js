'use strict';

angular.module('nixApp')
  .service('charts', function (bifrost,global,$q) {
    //Object chart estado
    var obChartE = function(chart){
      var label = [];
      var data = [];
      var series = ['Nuevo','En proceso','Pendiente cierre','Cerrado'];

      var ter ={
        N :[],
        P :[],
        V :[],
        C :[]
      };
      return $q(function(resolve){
        bifrost.s({
          'table' : chart.table,
          'val' : '*'
        }).then(function(response){
          var ids = [];
          for (var key in response){
            ids.push(response[key][chart.id]);
            var nom = ((response[key][chart.label]).split(' '));
            label.push(nom[0] + ' ' +nom[1]);
          }
          var promises = [];

          angular.forEach(ids, function(v, i){
            var pro = $q(function(resolve){
              bifrost.rE({
                'Procedure' : 'eTicketos',
                'val' : global.rQ([
                  'fk_'+chart.id,
                  ids[i],
                  '1=1'
                ])
              }).then(function(response2){
                resolve(response2);
              });
            });

            promises.push(pro);
          });

          $q.all(promises).then(function(resp){
            for (var i = 0; i < resp.length; i++) {
              for (var k in resp[i][0]) {
                ter[k].push(resp[i][0][k]);
              }
            }

            data = $.map(ter, function(value) {
                return [value];
            });
            var ob = {
              'data' : data,
              'labels' : label,
              'series' : series
            };
            resolve(ob);
          });

        });
      });
    };
    //Object chart Date
    var obChartD = function(chart){
      //1. Verifica si hay sede
      var Proc = false;
      var Func = false;
      var val = '';

      if(chart.idSede){
        //2. Verifica si hay mes
        if(chart.month){
          //eTickPerMesSede function
          Func = 'eTickPerMesSede('+ global.rQ([chart.month,chart.year,chart.idSede]) + ')';
        }else {
          //eTickPerAnoSede procedure
          Proc = 'eTickPerAnoSede';
          val = chart.year;
        }
      }else {
        //2. Verifica si hay mes
        if(chart.month){
          //eTickPerMes
          Func = 'eTickPerMes('+ global.rQ([chart.month,chart.year]) + ')';
        }else {
          //console.log(chart);
          //eTickPerAno
          Proc = 'eTickPerAno';
          val = chart.year;
        }
      }

      var ob = {
        labels: [],
        series:['serie'],
        data : []
      };

      return $q(function(resolve){
        if(Func){ //MES --- LABEL : DIAS
          bifrost.s({
            'table' : Func,
            'val' : '*'
          }).then(function(response){
            var temp = [];
            for (var i = 0; i < response.length; i++) {
              ob.labels.push(response[i].dia);
              temp.push(response[i].cantidad);
            }
            ob.data.push(temp);

            resolve(ob);
          });
        }else { // AÑO ----- LABEL : MESES
          bifrost.rE({
            'Procedure' : Proc,
            'val' : val
          }).then(function(response){
            var temp = [];
            for (var i = 0; i < response.length; i++) {
              ob.labels.push(response[i].mes);
              temp.push(response[i].cantidad);
            }
            ob.data.push(temp);

            resolve(ob);
          });
        }
      });
    };

    return {
      oCE : obChartE,
      oCD : obChartD
    };
  });
