'use strict';

angular.module('nixApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'ui.router',
  'lumx',
  'ngMaterial',
  'ngMessages',
  'datatables',
  'datatables.columnfilter',
  'chart.js',
  'ngMdIcons'
])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider,$mdThemingProvider) {
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode(true);
  $mdThemingProvider.theme('default').primaryPalette('cyan').accentPalette('grey');

  Chart.defaults.global.colours[0] = '#FF9800';
  Chart.defaults.global.colours[1] = '#673AB7';
  Chart.defaults.global.colours[2] = '#00BCD4';
  Chart.defaults.global.colours[3] = '#CDDC39';
  Chart.defaults.StackedBar.showTotal = true;

}).directive('chartStackedBar', function (ChartJsFactory) {
    return new ChartJsFactory('StackedBar');
}).constant('COLORES', {
	N : '#FF9800',
  P : '#673AB7',
  V : '#00BCD4',
  C : '#CDDC39',
  H : '#E0E0E0'

});
